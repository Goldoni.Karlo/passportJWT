const express = require('express');
const app = express();
var mongoose = require('mongoose');

// override with POST having ?_method
var methodOverride = require('method-override');
app.use(methodOverride('_method'));


// Use URL encoded
app.use(express.urlencoded({
    extended: false
}));

// Use Body-parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// Use Mongoose 
mongoose.connect('mongodb://localhost/warehouse');


// Use models
require('./models/users');
require('./models/categories');
require('./models/products');
require('./models/sessions');


// Passport:
const passport = require('./middleware/passportInit');
app.use(passport.initialize());
//app.use(passport.authenticate('jwt'));
//session



// Use routes
app.use(require('./routes'));

// Use static files
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'pug');

const port = process.env.PORT ? process.env.PORT : 3001;


app.listen(port, function () {
    console.log('Example app listening on port' + port);
});
