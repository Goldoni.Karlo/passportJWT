const mongoose = require('mongoose');
const users = mongoose.model('users');
const mainModel = users;

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

const jwt = require('jsonwebtoken');
const jwtsecret="FigVamVsemANeMySecretKey";

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, function(username, password,done){
  console.log ('LocalStategy is used');
    mainModel.findOne({ email : username},function(err,user){
    return err 
      ? done(err)
      : user
        ? password === user.password
          ? done(null, user)
          : done(null, false, { message: 'Incorrect password.' })
        : done(null, false, { message: 'Incorrect username.' });
  });
}));

passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer') ,
        secretOrKey   : jwtsecret
    },
    function (jwtPayload, cb) {
      mainModel.findOne({ _id: jwtPayload.id }).exec()
      .then((user) => {
        return cb(null, user);
      })
      .catch((err) => {
            console.log (err);
            return cb(err);
      });
  }
));

passport.getToken = function (userObj, ttl)
{
  const payload = {
     id: userObj._id,
     firstname: userObj.firstname,
     lastname: userObj.lastname, 
     email: userObj.email,
    };
    return jwt.sign(payload, jwtsecret, { expiresIn: Date.now()+ttl*1000 });
    
   // return jwt.sign(payload, jwtsecret, {expiresIn: Date.now()+ttl*1000});    
}

passport.showToken = function (token)
{
  return jwt.decode(token);    
}


passport.serializeUser(function(user, cb) {
  cb(null, user.email);
});

passport.deserializeUser(function(id, cb) {
  mainModel.findById(id, function(err, user) {
    cb(err, user);
  });
});

 passport.authenticationMiddleware= function (req, res, next) {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err || !user) {
        return res.status(400).json({
        message: info.message
      });
    }
    req.login(user, { session: false }, (error) => {
      if (error) {
        res.send(error);
      }
        req.user={'id':user._id, 'email':user.email};
        next();
    });
  })(req, res);
}

module.exports = passport;