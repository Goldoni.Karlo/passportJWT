const mongoose = require('mongoose');
const users = mongoose.model('users');
const mainModel = users;

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, function(username, password,done){
  mainModel.findOne({ email : username},function(err,user){
    return err 
      ? done(err)
      : user
        ? password === user.password
          ? done(null, user)
          : done(null, false, { message: 'Incorrect password.' })
        : done(null, false, { message: 'Incorrect username.' });
  });
}));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  mainModel.findById(id, function(err, user) {
    cb(err, user);
  });
});

passport.authenticationMiddleware=function (req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        res.status(401).send('Not authorized!');
};

module.exports = passport;