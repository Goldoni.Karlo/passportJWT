function validateBodyEmpty(req, arr) {
    // Check body have empty parameters from arr [namt1, name2,... name n]
    var errstr = '';
    for (var key in req.body) {
        if (arr.indexOf(key) !== -1) {
            if (req.body[key].length == 0) {
                errstr += ' Parameter ' + key + ' is empty ';
            }
        }
    }
    return errstr;
}

module.exports = validateBodyEmpty;