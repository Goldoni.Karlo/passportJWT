var mongoose = require('mongoose');

  var sessionsSchema = new mongoose.Schema({
    session: {
      lastAccess: Date,
      cookie: {
        originalMaxAge: Date,
        expires: Date,
        httpOnly: Boolean,
        path: String
      },
      "_csrf": String
    },
    expires: Date
  });
  

// Requires population of author
sessionsSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    body: this.body,
    createdAt: this.createdAt
    };
};

mongoose.model('sessions', sessionsSchema);