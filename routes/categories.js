var mongoose = require('mongoose');
var router = require('express').Router();
const categories = mongoose.model('categories');
const products = mongoose.model ('products');
const users = mongoose.model ('users');

// mainModel, slaveModel, upperModel is for lihgtway to use standart route for linked objects
const mainModel = categories;
const slaveModel = products;
const upperModel = users;

const validateBodyHave = require ('./../middleware/validateBodyHave');
const validateBodyEmpty = require ('./../middleware/validateBodyEmpty');

const passport = require('./../middleware/passportInit');

router.get('/', function (req, res, next) {
     passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
    mainModel.find()
        .exec()
        .then(function (obj) {
            if (obj == undefined) {
                throw ('No objects in DB')
            }
            return res.status(200).send(obj);
        })
        .catch(err => {
            res.status(404).send(err);
        });
});


router.get('/:_id', function (req, res, next) {
 passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
    mainModel.findOne({
            _id: req.params._id
        })
        .exec()
        .then(function (obj) {
            if (obj == undefined) {
                throw ('Object with _id=' + req.params._id + ' not found');
            }
            return res.status(200).send(obj);
        })
        .catch(err => {
            res.status(404).send(err);
        });
});

router.delete('/:_id', function (req, res, next) {
     passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
    // Check _id is real in request
    deleteObj = null;
    mainModel.findOne({_id: req.params._id}) .exec()
        .then(function (obj) {
            if (obj == undefined) {
                throw ('No object with _id= ' + req.params._id + ' for deleting');
            }
            deleteObj = obj;
            return mainModel.findOneAndDelete({
                    _id: obj._id
                })
                .exec()
                .then(function (obj) {
                    return res.status(200).send('Object with _id=' + obj._id + ' deleted succesfully');
                })
        })
        .catch(err => {
            res.status(404).send(err);
        })
});

router.get('/:_id/products', function (req, res, next) {
     passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
    let category = null;
    mainModel.findOne({_id: req.params._id}).exec()
        .then(foundCategory => {
            if (foundCategory == null) {
                throw ('Object with _id=' + req.params._id + ' not found');
            }
            category = foundCategory;
            return slaveModel.find({category: category._id}).exec();
        })
        .then(foundProducts => {
            if (foundProducts.length === 0) {
                throw ('No products in category object by _id=' + category._id)
            };
            return res.status(200).send(foundProducts);
        })
        .catch(err => {
            res.status(404).send(err);
        });
});


router.post('/', function (req, res, next) {
     passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
        // Create new Object from body  {name:'value'}
       let newObj = null;
       next();
    }, function (req, res, next) {
        // Check body for right
        let errstr = validateBodyHave(req, ['name', 'user']);
        if (errstr) {
            return res.status(404).send(errstr);
        }
        next();
    }, function (req, res, next) {
        // Check body for empty
        let errstr = validateBodyEmpty(req, ['name', 'user']);
        if (errstr) {
            return res.status(404).send(errstr);
        }
        next();
    }, function (req, res) {
        // Check user is real
        upperModel.findOne({_id: req.body.user}).exec()
            .then(function (obj) {
                if (obj == null) {
                    throw ('User is not exists');
                }
                // Check name is unique
                return mainModel.findOne({name: req.body.name}).exec()
            })
            .then(function (obj) {
                            if (obj !== null) {
                            throw ('Category name is allready registered');
                            }
                            newObj = new mainModel({
                            name: req.body.name,
                            user: req.body.user
                            });
                            return newObj.save()
            })
            .then(function () {
                            return res.status(200).send(newObj + ' created succesfully');
            })
            .catch(err => {
            res.status(404).send(err);
            })
});


router.patch('/:_id', function (req, res, next) {
     passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
    // Updating some fields of Object from body  {name:'value'}
    mainModel.findOne({_id: req.params._id}).exec()
                .then(function (obj) {
                        if (obj == undefined) {
                        throw ('Object for patching by _id=' + req.params._id + ' not exists');
                        }
                        return mainModel.findOneAndUpdate({_id: req.params._id},{$set:req.body}).exec()
                })
                .then(function () {
                        return res.status(200).send('Patching object with _id=' + req.params._id + ' is done succesfully');
                })
                .catch(err => {
                    res.status(404).send(err);
                })
});


router.put('/:_id', function (req, res, next) {
     passport.authenticationMiddleware(req, res, next);
}, function(req, res, next) { 
// Replace some fields of Object from body  {name:'value'}
    mainModel.findOne({_id: req.params._id}).exec()
                .then(function (obj) {
                    if (obj == undefined) {
                        throw ('Object for replacing by _id=' + req.params._id + ' not exists');
                    }
                    return mainModel.replaceOne({_id: req.params._id}, req.body).exec()
                })
                .then(function () {
                        return res.status(200).send('Replacing object with _id=' + req.params._id + ' is done succesfully');
                })
                .catch(err => {
                    res.status(404).send(err);
                })
});

module.exports = router;