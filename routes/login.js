var router = require('express').Router();
const passport = require('./../middleware/passportInit');
var mongoose = require('mongoose');
const users = mongoose.model('users');
// mainModel is for lihgtway to use standart route for many object
const mainModel = users;


router.post('/', 
  passport.authenticate('local', { failureStatus: 403 }),
  function(req, res, next) {
                           // res.status(200).send('Ok, generating Token for '+req.body.email);
      const query = async (req, res)=> {
         const response = await mainModel.findOne({email: req.body.email}).exec();
         req.user=response;
         next();
      }
     query(req, res);
  }, function (req, res) {
     const token=passport.getToken(req.user, 10000);
    // console.log ('token= ',token);
    // console.log ('token= ',passport.showToken(token));
    res.status(200).json({ user: req.user , jwtToken: token });    
});

module.exports = router;